#!/bin/bash
set -e
sleep ${START_DELAY_WAIT:-0}
build/ticker --thread_count=${THREAD_COUNT:-3} --postgresHostIP=${POSTGRES_HOST_IP:-} --postgresPort=${POSTGRES_PORT:-} --postgresUser=${POSTGRES_USER:-postgres} --postgresPassword=${POSTGRES_PASSWORD:-password} --postgresDBName=${POSTGRES_DB_NAME:-postgres} --postgresSchemas=${POSTGRES_SCHEMAS:-cognition}