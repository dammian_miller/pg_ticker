FROM ubuntu:16.04

ENV DEBIAN_FRONTEND noninteractive

WORKDIR /opt

# Build tools
RUN apt update && \
    apt install -y \
    sudo \
    tzdata \
    git \
    cmake \
    cmake-curses-gui \
    wget \
    curl \
    unzip \
    libssl-dev \
    build-essential

# Parallelism and linear algebra libraries:
RUN apt install -y \
    libtbb-dev \
    libeigen3-dev

# Python:
RUN apt install -y \
    python3-dev \
    python3-tk \
    python3-numpy

# GCC7
RUN apt-get install -y software-properties-common \
    && add-apt-repository ppa:ubuntu-toolchain-r/test \
    && apt update \
    && apt install g++-7 -y \
    && update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7 60 \
    --slave /usr/bin/g++ g++ /usr/bin/g++-7 \
    && update-alternatives --config gcc

RUN apt install -y curl zip unzip tar
# VCPKG
RUN mkdir ~/.vcpkg \
    && touch ~/.vcpkg/vcpkg.path.txt

RUN git clone https://github.com/Microsoft/vcpkg.git \
    && cd vcpkg \
    && ./bootstrap-vcpkg.sh \
    && ./vcpkg integrate install \
    && ln -s /opt/vcpkg/vcpkg /usr/bin/vcpkg

RUN apt update && apt install -y libboost-all-dev

# PostgreSQL 12 Headers
RUN apt install -y postgresql-common \
    # && yes | sh /usr/share/postgresql-common/pgdg/apt.postgresql.org.sh \
    && apt install -y --allow-unauthenticated libpqxx-dev libpq-dev postgresql-server-dev-all

# CMAKE 3.17
RUN wget https://github.com/Kitware/CMake/releases/download/v3.17.0/cmake-3.17.0.tar.gz \
    && tar xvfz cmake-3.17.0.tar.gz \
    && cd cmake-3.17.0 \
    && ./configure \
    && make -j$(nproc) \
    && make install

ENV CXXFLAGS -I/usr/include/postgresql -I/opt/vcpkg/installed/x64-linux/include -std=c++11 -DPQXX_HIDE_EXP_OPTIONAL

# Cognition Ticker
RUN git clone --recurse-submodules https://dammian_miller@bitbucket.org/dammian_miller/pg_ticker.git \
    && cd pg_ticker \
    && mkdir build && cd build \
    && cmake .. \
    && make -j$(nproc)

WORKDIR /opt/pg_ticker
SHELL ["/bin/bash", "-c"]
CMD ["/bin/bash", "-c", "./run.sh"]
