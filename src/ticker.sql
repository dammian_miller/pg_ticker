---------------------------------------------
-- define cognition jobs tables, sequences --
---------------------------------------------


drop sequence if exists job_ids cascade;
create sequence job_ids;
alter sequence job_ids restart with 1;


drop table if exists job cascade;
create table job(
  id int default nextval('job_ids') not null unique,
  name text default currval('job_ids') not null unique,
  query text not null,
  session_query text not null default '',
  iterations int default 10,
  interval int4 default 1000,
  retries int default 0,
  reload int default 1,
  active int default 1,
  timeout int default 1000,
  listen int default 0,
  listen_channel text default '',
  listen_params_count int default 2,
  retry_on_fail integer default 0 not null
);