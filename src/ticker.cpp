#include <getopt.h>

#include <sstream>
#include <iostream>
#include <thread>    
#include <vector>
#include <algorithm>
#include <cstring>
#include <pqxx/pqxx>
#include <libpq-fe.h>
#include <netinet/in.h>

// #include <city.h>

#include <limits>
#include <inttypes.h>

#include <boost/thread/thread.hpp>
#include "libpq-pool/pgbackend.h"
#include <chrono>

using namespace std::chrono;

// https://codereview.stackexchange.com/questions/80386/packing-and-unpacking-two-32-bit-integers-into-an-unsigned-64-bit-integer
uint64_t combine(uint32_t low, uint32_t high)
{
     return (((uint64_t) high) << 32) | ((uint64_t) low);
}

uint32_t high(uint64_t combined)
{
    return combined >> 32;
}

uint32_t low(uint64_t combined)
{
    uint64_t mask = std::numeric_limits<uint32_t>::max();
    return mask & combined; // should I just do "return combined;" which gives same result?
}

// https://stackoverflow.com/questions/22648978/c-how-to-find-the-length-of-an-integer
int int_length(int i) {
    int l=0;
    for(;i;i/=10) l++;
    return l==0 ? 1 : l;
}

int int64_length(int64_t i) {
    int l=0;
    for(;i;i/=10) l++;
    return l==0 ? 1 : l;
}

// https://stackoverflow.com/questions/9695720/how-do-i-convert-a-64bit-integer-to-a-char-array-and-back
void int64ToChar(char mesg[], int64_t num) {
    *(int64_t *)mesg = htonl(num);
}

// https://stackoverflow.com/questions/24538954/how-do-you-cast-a-uint64-to-an-int64
uint64_t Int64_2_UInt64(int64_t value)
{
     return (((uint64_t)((uint32_t)((uint64_t)value >> 32))) << 32) 
        | (uint64_t)((uint32_t)((uint64_t)value & 0x0ffffffff));           
}

int64_t UInt64_2_Int64(uint64_t value)
{
    return (int64_t)((((int64_t)(uint32_t)((uint64_t)value >> 32)) << 32) 
       | (int64_t)((uint32_t)((uint64_t)value & 0x0ffffffff)));           
}

int32_t uint32_to_int32(uint32_t value)
{
	int32_t tmp;
	std::memcpy(&tmp, &value, sizeof(tmp));
	return value;//tmp;
}

int64_t uint64_to_int64(uint64_t value)
{
	int64_t tmp;
	std::memcpy(&tmp, &value, sizeof(tmp));
	return tmp;
}

char *myhton(char *src, int size) {
  char *dest = (char *)malloc(sizeof(char) * size);
  switch (size) {
  case 1:
    *dest = *src;
    break;
  case 2:
    *(int16_t *)dest = htobe16(*(int16_t *)src);
    break;
  case 4:
    *(int32_t *)dest = htobe32(*(int32_t *)src);
    break;
  case 8:
    *(int64_t *)dest = htobe64(*(int64_t *)src);
    break;
  default:
    *dest = *src;
    break;
  }
  memcpy(src, dest, size);
  free(dest);
  return src;
}

char *umyhton(char *src, int size) {
  char *dest = (char *)malloc(sizeof(char) * size);
  switch (size) {
  case 1:
    *dest = *src;
    break;
  case 2:
    *(uint16_t *)dest = htobe16(*(uint16_t *)src);
    break;
  case 4:
    *(uint32_t *)dest = htobe32(*(uint32_t *)src);
    break;
  case 8:
    *(uint64_t *)dest = htobe64(*(uint64_t *)src);
    break;
  default:
    *dest = *src;
    break;
  }
  memcpy(src, dest, size);
  free(dest);
  return src;
}

// https://www.techiedelight.com/get-slice-sub-vector-from-vector-cpp/
template<typename T>
std::vector<T> vec_slice(std::vector<T> const &v, int s, int e)
{
	auto first = v.cbegin() + s;
	auto last = v.cbegin() + e;
	std::vector<T> vec(first, last);
	return vec;
}
bool isNumber(const std::string& str)
{
    for (char const &c : str) {
        if (std::isdigit(c) == 0) return false;
    }
    return true;
}

class ticker
{
public:
	ticker(std::string &db_conn_str_, int &db_pool_size_) : db_conn_count(db_pool_size_), db_conn_str(db_conn_str_)
	{
		pgbackend = std::make_shared<PGBackend>(db_conn_str, db_conn_count);
		start_jobs();
	}

	void start_jobs()
	{
		std::vector <std::string> job_ids;
		auto conn = pgbackend->connection();
		PGresult *res = NULL;
		res = PQexec(conn->connection().get(), "select id from job");
		int rows = PQntuples(res);
		std::string job_id = "0";
		for(int i = 0; i < rows; i++)
		{
			job_id = PQgetvalue(res, i, 0);//(int)atoi(
			// if(job_id < std::numeric_limits<long>::min() 
			// 	|| job_id > std::numeric_limits<long>::max()
			// 	|| job_id == NULL) 
			// 	continue;
			std::cout << "Adding job_id: " << job_id << std::endl;
			job_ids.push_back(job_id);
		}
		PQclear(res);
		pgbackend->freeConnection(conn);

		pgbackend = std::make_shared<PGBackend>(db_conn_str, rows);		
		std::vector<std::thread> threads(rows);
		// job_id = "0";
		for(int i = 0; i < rows; i++)
		{
			// job_id = (int)job_ids.at(i);
			// std::cout << "Starting job: " << job_id << std::endl;
			threads[i] = std::thread(boost::bind(&ticker::run_job, this, _1), job_ids.at(i));
 		}
		for (auto& th : threads) {
			th.join();
		}
	}

	int run_job(std::string job_id)
	{
		if(job_id == "") 
			return 0;
		// std::cout << "Starting job: " << job_id << std::endl;
		

		// char numString[bufsize] = { };
		// snprintf(str, bufsize - 1, "%ld", job_id);

		// std::string s = std::to_string(job_id);
		// char const *pchar = s.c_str(); 

		// std::stringstream tmp;
		// try{
    	// 	tmp << job_id;
		// }catch(const std::exception& e){
		// 	std::cout << "Error: " << e.what() << std::endl;
		// }

		// std::cout << "sizeof(job_id): " << sizeof(job_id) << std::endl;
		const char *paramValues[1];
		const char *str = job_id.c_str();
		paramValues[0] = str;

		auto conn = pgbackend->connection();
		PGresult *res = NULL;

    // char const *num_char = tmp.str().c_str();

	// 	const int bufsize = 100;
	// 	const char *paramValues[1];
	// 	char str[bufsize] = { };
	// 	snprintf(str, bufsize - 1, "%d", (int)job_id);

	// 	paramValues[0] = str;  

		 
  
		// // create a new object of stringstream class
		// std::stringstream stream;
	
		// // add long number to variable of type stringstream
		// stream << job_id;
		// std::string str;
	
		// // get string object from stringstream variable using
		// // str() method
		// str = stream.str();
		// if(str.empty())
		// 	return 0;
		// // cout << long_to_string.c;
		// paramValues[0] = str.c_str();

		char *stm = "select id, name, query, iterations, interval, reload, active, timeout, listen, listen_channel, listen_params_count, session_query, retry_on_fail from job where id = $1";
		res = PQexecParams(conn->connection().get(), stm, 1, NULL, paramValues, NULL, NULL, 0);

		high_resolution_clock::time_point current_time = high_resolution_clock::now();
		high_resolution_clock::time_point new_time = high_resolution_clock::now();
		auto query_duration = duration_cast<milliseconds>( new_time - current_time ).count();
		long wait_time = 0;

		if (PQresultStatus(res) != PGRES_TUPLES_OK) {
			std::cout << "Select jobs failed: " << PQresultErrorMessage(res) << std::endl;
		} else {
			long id = (long)atol(PQgetvalue(res, 0, 0));
			std::string name = PQgetvalue(res, 0, 1);
			std::string query = PQgetvalue(res, 0, 2);
			long iterations = (long)atol(PQgetvalue(res, 0, 3));
			int interval = (int)atoi(PQgetvalue(res, 0, 4));
			int reload = (int)atoi(PQgetvalue(res, 0, 5));
			int active = (int)atoi(PQgetvalue(res, 0, 6));
			std::string timeout_query = "set statement_timeout to ";
			timeout_query.append(PQgetvalue(res, 0, 7)).append(";show statement_timeout;");
			timeout_query.append(query);
			int listen = (int)atoi(PQgetvalue(res, 0, 8));
			std::string listen_channel = PQgetvalue(res, 0, 9);
			int listen_params_count = (int)atoi(PQgetvalue(res, 0, 10));
			std::string session_query = PQgetvalue(res, 0, 11);
			int retry_on_fail = (int)atoi(PQgetvalue(res, 0, 12));
			PGnotify *notify;
			if(listen > 0 && active > 0)
			{
				std::string sql = ("LISTEN " + listen_channel);
				res = PQexec(conn->connection().get(), sql.c_str());
				if (PQresultStatus(res) != PGRES_COMMAND_OK)
				{
					fprintf(stderr, "LISTEN command failed: %s", PQerrorMessage(conn->connection().get()));
					PQclear(res);
					pgbackend->freeConnection(conn);
				}
				// should PQclear PGresult whenever it is no longer needed to avoid memory leaks
				PQclear(res);
				while (true)
				{
					/*
					* Sleep until something happens on the connection.  We use select(2)
					* to wait for input, but you could also use poll() or similar
					* facilities.
					*/
					int sock;
					fd_set input_mask;

					sock = PQsocket(conn->connection().get());

					if (sock < 0)
						break; /* shouldn't happen */

					FD_ZERO(&input_mask);
					FD_SET(sock, &input_mask);

					if (select(sock + 1, &input_mask, NULL, NULL, NULL) < 0)
					{
						fprintf(stderr, "select() failed: %s\n", strerror(errno));
						pgbackend->freeConnection(conn);
						exit(1);
					}

					/* Now check for input */
					new_time = high_resolution_clock::now();
					PQconsumeInput(conn->connection().get());
					while ((notify = PQnotifies(conn->connection().get())) != NULL)
					{
						std::string id;
						std::string val;
						std::stringstream payload;
						payload << notify->extra;
						if( payload.good() )
						{
							const char *paramValues[listen_params_count];
							std::string substr;
							for(uint i = 0; i < listen_params_count && !payload.eof(); i++)
							{
								getline( payload, substr, ',' );
								char * cstr = new char [substr.length()+1];
								std::strcpy (cstr, substr.c_str());
								paramValues[i] = cstr;
								// std::cout << "paramValues[" << i << "]: " << paramValues[i] << std::endl;		
							}
							res = PQexecParams(conn->connection().get(), query.c_str(), listen_params_count, NULL, paramValues, NULL, NULL, 0);
							PQclear(res);
						}
						PQfreemem(notify);
					}
					current_time = high_resolution_clock::now();
					query_duration = duration_cast<milliseconds>( current_time - new_time ).count();
					wait_time = interval - query_duration;
					// std::cout << "Job name: " << name << " ID:" << std::to_string(job_id) << " active: " << std::to_string(active) << " query_duration: " << std::to_string(query_duration) << "ms wait_time: " << std::to_string(wait_time) << "ms" << std::endl;
					std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));
				}
			}
			else
			{
				new_time = high_resolution_clock::now();
				if(active > 0)
				{
					res = PQexec(conn->connection().get(), session_query.c_str());
					PQclear(res);
				}
				current_time = high_resolution_clock::now();
				query_duration = duration_cast<milliseconds>( current_time - new_time ).count();
				wait_time = interval - query_duration;
				// std::cout << "Job name: " << name << " ID:" << job_id << " session_query: " << session_query << " query_duration: " << std::to_string(query_duration) << "ms wait_time: " << std::to_string(wait_time) << "ms" << std::endl;
				std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));
				for(uint i = 0; i < iterations; i++)
				{
					new_time = high_resolution_clock::now();
					if(active > 0)
					{
						res = PQexec(conn->connection().get(), timeout_query.c_str());
						PQclear(res);
						current_time = high_resolution_clock::now();
						query_duration = duration_cast<milliseconds>( current_time - new_time ).count();
						wait_time = interval - query_duration;
						if(retry_on_fail > 0 && PGRES_COMMAND_OK != PQresultStatus(res))
						{
							i--;
							wait_time = 0;
						}
						// std::cout << "Job name: " << name << " ID:" << job_id << " active: " << std::to_string(active) << " query_duration: " << std::to_string(query_duration) << "ms wait_time: " << std::to_string(wait_time) << "ms" << std::endl;
						std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));
					}
				}
				if(reload > 0)
				{
					pgbackend->freeConnection(conn);
					return ticker::run_job(job_id);
					// ticker::run_job(job_id);
					// std::thread thread;
					// std::thread(boost::bind(&ticker::run_job, this, _1), job_id);
					// thread.join();
					// return boost::bind(&ticker::run_job, this, job_id);
				}
			}
		}
		pgbackend->freeConnection(conn);
		return 0;

	}

private:

	int db_conn_count;

	std::string db_conn_str;

	std::shared_ptr<PGBackend> pgbackend;

};

int main(int argc, char **argv)
{
	int thread_count = 4;
	int opt = 0;
	
	std::string postgresHostIP = "";
	std::string postgresPort = "6432";
	std::string postgresDBName = "postgres";
	std::string postgresUser = "postgres";
	std::string postgresPassword = "password";
	std::string postgresSchemas = "public";
	std::string client_encoding = "UTF8";

	const char *opts = "+"; // set "posixly-correct" mode
	const option longopts[]{
		{"postgresHostIP", 1, 0, 'a'},
		{"postgresPort", 1, 0, 'b'},
		{"postgresDBName", 1, 0, 'c'},
		{"postgresUser", 1, 0, 'd'},
		{"postgresPassword", 1, 0, 'e'},
		{"thread_count", 1, 0, 'f'},
		{"client_encoding", 1, 0, 'g'},
		{"postgresSchemas", 1, 0, 'h'},
		{0, 0, 0, 0}}; 

	while ((opt = getopt_long_only(argc, argv, opts, longopts, 0)) != -1)
	{
		switch (opt)
		{
		case 'a':
			std::cout << "setting postgresHostIP: " << optarg << std::endl;
			postgresHostIP = optarg;
			break;
		case 'b':
			std::cout << "setting postgresPort: " << optarg << std::endl;
			postgresPort = optarg;
			break;
		case 'c':
			std::cout << "setting postgresDBName: " << optarg << std::endl;
			postgresDBName = optarg;
			break;
		case 'd':
			std::cout << "setting postgresUser: " << optarg << std::endl;
			postgresUser = optarg;
			break;
		case 'e':
			std::cout << "setting postgresPassword: " << optarg << std::endl;
			postgresPassword = optarg;
			break;
		case 'f':
			std::cout << "setting thread count: " << optarg << std::endl;
			thread_count = atoi(optarg);
			break;
		case 'g':
			std::cout << "setting client_encoding: " << optarg << std::endl;
			client_encoding = optarg;
			break;
		case 'h':
			std::cout << "setting postgresSchemas: " << optarg << std::endl;
			postgresSchemas = optarg;
			break;			
		default: 
			exit(EXIT_FAILURE);
		}
	}

	std::cout << "Starting Cognition Ticker..." << std::endl;

	// defaults to Unix socket - 30% faster than TCP
	std::string db_conn_str = "dbname='" + postgresDBName + "' user='" + postgresUser + "' password='" + postgresPassword + "' options='-c search_path=" + postgresSchemas + "'";
	// only if host IP specified then switch to TCP
	if(postgresHostIP.length() > 0)
	{
		db_conn_str = "postgresql://" + postgresUser + ":" + postgresPassword + "@" + postgresHostIP + ":" + postgresPort + "/" + postgresDBName + "?options=-c%20search_path%3D" + postgresSchemas;
	}
	std::cout << db_conn_str << std::endl;

	ticker(db_conn_str, thread_count);
	
	return 0;
}
